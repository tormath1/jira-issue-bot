### Simple Go Application

The goal of this `go` application is to check issue status on `JIRA`: if an issue is not active since weeks, watchers will be notify that this issue will be soon closed.

# Getting started

Please make sure that [Docker](https://www.docker.com/) is installed on your machine. Then you can clone this repo.

```shell
$ docker version
$ git clone https://gitlab.com/TortueMat/jira-issue-bot
$ cd jira-issue-bot/
```

Now, you can `build` locally your `docker` image, for testing purpose:

```shell
$ docker-compose build -f docker-compose-development.yaml --no-cache
$ docker images
```

Or you can pull directly the latest release image from `registry.gitlab.com`, assert you're logged into the Gitlab registry.

```shell
$ docker login registry.gitlab.com
$ docker-compose pull
```

# Create templates

You can create template for posting comments. You just need to add it in `templates/`. There is an example if you want to check. Please make sur to share it with `Docker` when you will fire up your application (`-v "templates/:/templates"`)

# Run your application

Before running your application, you need to update `configuration/jira.yaml` with your  own configuration. Then

```shell
$ docker-compose up -d
$ docker-compose logs
```

You can `scale` your application by pulling latest image of `jira-issue-bot` and restart your service.

```shell
$ docker-compose pull
$ docker-compose restart
```

# Metrics

This `go` project has embedded Prometheus `metrics`, they are pushed on a `PushGateway` provided by [Prometheus](https://www.prometheus.io). So, If you want to scrap latest metrics, you need to supply a push Gateway. You can specify him address, by overriding `METRICS_GATEWAY` environment variable in `docker-compose.yaml`.
