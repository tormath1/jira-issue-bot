package issue

import "gitlab.com/jira-issue-bot/comment"

type Issue struct {
	Id       string            `json:"id"`
	Comments []comment.Comment `json:"comment,omitempty"`
}

func NewIssue(id string) Issue {
	/*
	  Create an issue
	  parameter: <string> Id of issue (i.e : "OPB-999")
	  return: An issue
	*/
	return Issue{
		Id: id,
	}
}
