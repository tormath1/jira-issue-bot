package main

import (
	"gitlab.com/jira-issue-bot/client"
	"gitlab.com/jira-issue-bot/comment"

	log "github.com/sirupsen/logrus"

	"net/http"
	"time"
)

func main() {
	jiraClient := client.NewJiraFromConfFile(&http.Client{})
	log.Infoln("Client created: ", jiraClient)

	commentToPost := comment.CreateCommentFromTemplate("comment_template.yaml")
	for {
		// Loop once a day
		/* Get all old issues non updated since lastUpdate days */
		old_issues := jiraClient.GetOldIssues(jiraClient.Options.LastUpdate)
		for _, issue := range old_issues {
			jiraClient.PostComment(commentToPost, issue)
		}

		/* Get all issues non updated since 3 days */
		old_issues_updated := jiraClient.GetOldIssues("3")
		for _, issue := range old_issues_updated {
			/* If our warning comment is the latest comment posted, we delete the issue */
			if issue.Comments[len(issue.Comments)-1].Body == commentToPost.Body {
				jiraClient.DeleteIssue(issue)
			}
		}
		time.Sleep(time.Second * 60 * 60 * 24)
	}
}
