package search

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestGetSearchJson(t *testing.T) {
	s := NewDefaultSearch("updated < -5d")
	expectedJson := []byte("{\"jql\":\"updated < -5d\",\"startAt\":0,\"maxResult\":50,\"fields\":[\"updated\",\"resolution\"]}\n")
	assert.Equal(t, s.GetJson(), expectedJson)
}
